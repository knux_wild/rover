require 'minitest/autorun'
require_relative '../lib/rover'

  class RoverTest < Minitest::Test
    describe SpaceMission::Rover do

      before do
        @rover = SpaceMission::Rover.new(x: 1, y: 2, facing: 'n')
        @rover_e = SpaceMission::Rover.new(x: 1, y: 2, facing: 'e')
        @rover_s = SpaceMission::Rover.new(x: 1, y: 2, facing: 's')
        @rover_w = SpaceMission::Rover.new(x: 1, y: 2, facing: 'w')
      end

      describe "Created rover" do
        it "should have starting position and methods to get them" do
          @rover.x.must_equal 1
          @rover.y.must_equal 2
          @rover.facing.must_equal "N"
        end

        it "should be able to return string with coordinates" do
          @rover.must_respond_to :coordinates
          @rover.coordinates.must_equal "1 2 N"
        end

        it "should be able to rotate" do
          @rover.must_respond_to :rotate
        end

        it "should be able to rotate right and spin 360" do
          @rover.rotate("R")
          @rover.facing.must_equal "E"

          4.times do
            @rover.rotate("R")
          end
          @rover.facing.must_equal "E"
        end

        it "should be able to rotate left and spin 360" do
          @rover.rotate("L")
          @rover.facing.must_equal "W"

          4.times do
            @rover.rotate("L")
          end
          @rover.facing.must_equal "W"
        end

        it "should be able to calculate next point" do
          @rover.must_respond_to :next_point
          @rover.next_point.must_equal({ x: 1, y: 3, facing: "N"})
          @rover_e.next_point.must_equal({ x: 2, y: 2, facing: "E"})
          @rover_s.next_point.must_equal({x: 1, y: 1, facing: "S"})
          @rover_w.next_point.must_equal({x: 0, y: 2, facing: "W"})
        end

        it "should be able to move in different directions" do
          @rover.move
          @rover.x.must_equal 1
          @rover.y.must_equal 3
          @rover.facing.must_equal "N"

          @rover_e.move
          @rover_e.x.must_equal 2
          @rover_e.y.must_equal 2
          @rover_e.facing.must_equal "E"

          @rover_s.move
          @rover_s.x.must_equal 1
          @rover_s.y.must_equal 1
          @rover_s.facing.must_equal "S"

          @rover_w.move
          @rover_w.x.must_equal 0
          @rover_w.y.must_equal 2
          @rover_w.facing.must_equal "W"
        end

      end
    end
  end

