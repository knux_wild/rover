require_relative "mission_errors"

module SpaceMission
  class Rover
    FACINGS = ['N', 'E', 'S', 'W']
    attr_accessor :x, :y, :facing

    def initialize(x:, y:, facing:)
      self.x = x
      self.y = y
      self.facing = facing.upcase
    end

    def rotate(direction)
      case direction.upcase
      when 'L' then rotate_left
      when 'R' then rotate_right
      else raise InvalidDirectionError.new
      end
    end

    def move
      case facing
      when 'N' then move_north
      when 'E' then move_east
      when 'S' then move_south
      when 'W' then move_west
      else raise WrongFacingError.new, "unable to move in #{facing} direction"
      end
    end

    def next_point
      case facing
      when 'N' then { x: x, y: y+1, facing: facing }
      when 'E' then { x: x+1, y: y, facing: facing }
      when 'S' then { x: x, y: y-1, facing: facing }
      when 'W' then { x: x-1, y: y, facing: facing }
      else raise WrongFacingError.new, "unknown facing #{facing}"
      end
    end

    def coordinates
      "#{x} #{y} #{facing}"
    end

    private

      def rotate_left
        new_facing_index = FACINGS.find_index(facing) - 1
        self.facing = FACINGS[new_facing_index]
      end

      def rotate_right
        # here % operation is used to make array cycled
        new_facing_index = (FACINGS.find_index(facing) + 1) % FACINGS.length
        self.facing = FACINGS[new_facing_index]
      end

      def move_north
        self.y += 1
      end

      def move_east
        self.x += 1
      end

      def move_south
        self.y -= 1
      end

      def move_west
        self.x -= 1
      end
  end
end
