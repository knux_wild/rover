module SpaceMission
  class MissionError < StandardError
  end

  class InvalidDirectionError < MissionError
  end

  class WrongFacingError < MissionError
  end

  class LandingError < MissionError
  end
end
