require_relative "rover"

module SpaceMission
  class Mission
    attr_accessor :borders
    attr_accessor :mission_data
    attr_accessor :rovers

    def initialize
      self.mission_data = []
      self.rovers = []
    end

    def read_data
      # reading borders
      self.borders = $stdin.readline.chomp.split(" ").map(&:to_i)

      # reading mission info
      mission_input = []
      while data = $stdin.readline.chomp and !data.empty?
        mission_input << data
      end

      # formatting info about rovers' missions
      mission_input.each_slice(2) do |data|
        sp_data = data[0].split(" ")
        sp = { x: sp_data[0].to_i, y: sp_data[1].to_i, facing: sp_data[2] }
        mission_data << { starting_point: sp, commands: data[1] }
      end
    end

    def start_mission
      mission_data.each do |data|
        land_rover data[:starting_point]
        move_rover(rovers.last, data[:commands])
      end
      rovers.each do |rover|
        p rover.coordinates
      end
    end

    private

      def land_rover(point)
        if point_available? point
          rovers << Rover.new(x: point[:x], y: point[:y], facing: point[:facing])
        else
          raise LandingError.new, "unable to land the rover to the point #{point[:x]}, #{point[:y]}"
        end
      end

      def move_rover(rover, commands)
        commands.each_char do |command|
          case command
            when 'M' then rover.move if point_available?(rover.next_point)
            else rover.rotate(command)
          end
        end
      end

      def point_available?(point)
        no_other_rovers?(point) && !out_of_borders?(point)
      end

      def no_other_rovers?(point)
        rovers.each do |rover|
          return false if (rover.x == point[:x] && rover.y == point[:y])
        end
        true
      end

      def out_of_borders?(point)
        point[:x] < 0 || point[:y] < 0 || point[:x] > borders[0] || point[:y] > borders[1]
      end

  end
end
