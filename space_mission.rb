require_relative 'lib/mission'

module SpaceMission
  p "Please enter the mission data and hit enter 2 times:"
  mission = Mission.new
  mission.read_data
  mission.start_mission
end
